#!/usr/bin/env ruby
require_relative 'array_extensions'
if RUBY_ENGINE == 'rbx' then
	require_relative 'rubinius_extensions'
end
require_relative 'sysaccess'
require 'fileutils'
require 'minitar'

$describescript_allowdialogbind = true
$describescript_substvars = true
begin
	require 'dialogbind'
rescue LoadError
	$describescript_allowdialogbind = false
end
$describescript_vars = { 'system' => ds_fetchos,
												 'args' => 'BAG-' + ARGV.clone.shift_l.join('|'),
												 'random' => rand(9999),
											   'ds_func_returnedv' => 0,
											   'pwd' => Dir.pwd }
$describescript_extensions = []
$describescript_allowblock = true
$describescript_stdlib_dir = '/tmp/ds-stdlib-' + rand(9999).to_s
$describescript_ifsucceeded = false
$describescript_currentlyfunction = false
$describescript_nativefunctions = []
$describescript_modulesdisabled = [ 'tk' ]
# TODO: support Windows

def bagtoarray_dup(bagobj)
	bagstr = bagobj[4..bagobj.length]
	if bagstr == nil then
		return []
	end
	out = []
	bagstr.split('|').each do |item|
		actual_item = item.gsub('[SPLITRP]', '|')
		if actual_item.to_i.to_s == actual_item then
			out.push(actual_item.to_i)
		else
			out.push(actual_item.to_s)
		end
	end
	return out
end

def dbgputs(string)
	if not ENV['DEBUG'] == nil then
		puts string.to_s
	end
end

def subst_vars(line, add_quotes=true)
	out = line.clone
	trailing_char = '"'
	if not add_quotes then
		trailing_char = ''
	end
	if $describescript_substvars then
		$describescript_vars.keys.each do |var|
			value_orig = $describescript_vars[var].to_s
			out = out.gsub('#{' + var.to_s + '}', trailing_char + value_orig.gsub('"', '') + trailing_char).clone
			if value_orig.start_with? 'BAG-' then
				count = -1
				bagtoarray_dup(value_orig).each do |item|
					count += 1
					out = out.gsub('#{' + var.to_s + '[' + count.to_s + ']}', trailing_char + item.to_s.gsub('[SPLITRP]', '|').gsub('"', '') + trailing_char).clone
				end
			end
		end
	else
		$describescript_substvars = false
	end
	return out
end

def subst_commands(cmd)
	if Array === cmd then
		out = []
		cmd.each do |item|
			out.push(subst_commands(item.to_s))
		end
		return out
	else
		out = cmd.clone
		if out.include?('%{') then
			main_command = out.get_all_commands[-1]
			if main_command == nil then
				return out
			end
			main_command_out = functional_part(main_command['cmd'])
			return out.replace_indices(main_command['start'], main_command['end'], '"' + main_command_out.to_s + '"')
		else
			return out
		end
	end
end

def functional_part(scmd)
	dbgputs "inline interpreter entered"
	cmd_subst = subst_commands(subst_vars(scmd))
	dbgputs "original line: #{scmd}"
	dbgputs "preprocessor converted line: #{cmd_subst}"
	cmd_split = cmd_subst.split_preserve_quotes(' ').gsub_everywhere([ [ '#{double_quote}', '"' ], ['#{single_quote}', '\'' ] ])
	cmd_first_orig = cmd_split.shift.clone.to_s
	cmd_first = cmd_first_orig.clone.downcase
	if cmd_split.include?('=>') || cmd_split.include?('&&') then
		dbgputs "strict commands sequence detected"
		sep = if cmd_split.include? '&&' then
							'&&'
		      else
							'=>'
		      end
		dbgputs "seperated with #{sep}"
		cmd_split.unshift(cmd_first)
		cmd_out = cmd_split.join_ml(' ').gsub('^', '[<SP>]').gsub(" #{sep} ", '^').gsub('"', '[<QV>]').split('^')
		dbgputs "cmd_out = #{cmd_out}"
		cmd_out.each do |cmd_r|
			result = functional_part(cmd_r.gsub('[<SP>]', '^').gsub('[<QV>]', '"').strip)
			dbgputs "command returned #{result}"
			if not Integer === result then
				dbgputs "result is not an integer, casting it to 0"
				result = 0
			end
			if result.to_i != 0 then
				return 1
			end
		end
		return 0
	end
	possible_op = 'dsop_' + cmd_split[0].to_s.downcase.gsub('=', 'equals').gsub('+', 'plus')
	possible_op = possible_op.clone.gsub('-', 'minus').gsub('/', 'to').gsub('*', 'times')
	possible_op = possible_op.clone.gsub('>', 'leftbig').gsub('<', 'leftsmall').gsub('!', 'not')
	dbgputs "potential operator: #{possible_op}"
	if cmd_first == 'out' || cmd_first == 'puts' || cmd_first == 'echo' then
		strprint = cmd_split.join(' ')
		puts strprint
		return strprint
	elsif cmd_first == 'no' then
		if cmd_split.join(' ').start_with? '#{' then
			return 0
		end
		return 1
	elsif cmd_first == 'disabled' then
		cmd_split.each do |item|
			if not $describescript_modulesdisabled.include? item then
				return 1
			end
		end
		return 0
	elsif cmd_first == 'system' then
		systemcmd = cmd_split.join(' ')
		return `#{systemcmd}`
	elsif cmd_first == 'function' || cmd_first == 'def' || cmd_first == 'routine' then
		$describescript_allowblock = false
		if $describescript_currentlyfunction then
			prettyerror(12, -1, 'A routine cannot be declared inside a routine.')
		end
		$describescript_currentlyfunction = true
		function_name = cmd_split.join('_').strip
		if function_name.length < 1 then
			return 0
		end
		if $describescript_nativefunctions.include_key? 'name', function_name then
			prettyerror(13, -1, 'A new routine cannot have the name of an already existing different routine.')
		elsif $describescript_extensions.include_key? 'key', function_name then
			prettyerror(14, -1, 'A new routine cannot have the name of a binded Ruby function.')
		end
		$describescript_nativefunctions.push({ 'name' => function_name, 'actions' => [] })
		return 1
	elsif cmd_first == 'if' || cmd_first == 'elif' then
		if cmd_first == 'elif' && $describescript_ifsucceeded then
			$describescript_allowblock = false
		else
			cmd_ran = cmd_split.join(' ').strip
			$describescript_allowblock = if functional_part(cmd_ran) == 0 then
																			true
																	 else
																		  false
																	 end
			$describescript_ifsucceeded = $describescript_allowblock.clone
			if $describescript_ifsucceeded then
				return 0
			end
		end
		return 1
	elsif cmd_first == 'return' then
		$describescript_vars['ds_func_returnedv'] = cmd_split.join(' ').strip
	elsif cmd_first == 'else' then
		if $describescript_ifsucceeded then
			return 1
		else
			$describescript_allowblock = true
			$describescript_ifsucceeded = false
			return 0
		end
	elsif cmd_first == 'bag' || cmd_first == 'list' then
		list_to_conv = []
		cmd_split.each do |item|
			if item.include?('-') then
				item_split = item.split('-')
				if item_split[0].to_i.to_s == item_split[0] && item_split[1].to_i.to_s == item_split[1] then
					list_to_conv = list_to_conv + eval(item_split[0] + '...' + item_split[1]).to_a
				else
					list_to_conv.push(item)
				end
			else
				list_to_conv.push(item.to_s)
			end
		end
		return 'BAG-' + list_to_conv.join('|')
	elsif cmd_first == 'invert' || cmd_first == '!' || cmd_first == 'not' then
		returned_out = functional_part(cmd_split.join(' ').strip)
		if Integer === returned_out then
			if returned_out != 0 then
				return 0
			end
		else
			if returned_out == '' || returned_out == 'BAG-' then
				return 0
			end
		end
		return 1
	elsif cmd_first == 'extend' || cmd_first == 'extend_relative' || cmd_first == 'extend_stdlib' then
		if cmd_split.length < 1 then
			raise 'Cannot extend nil.'
		end
		modulev = 'fileutils'
		if cmd_split[0].downcase == 'from' then
			modulev = cmd_split[1]
			cmd_split.shift(2)
		end
		if modulev == nil then
			raise 'Module is nil, probably not enough arguments.'
		end
		funcs = []
		cmd_split.each do |func|
			$describescript_extensions.push({ 'func' => func, 'module' => modulev })
			funcs.push(func)
		end
		begin
			if cmd_first == 'extend_relative' then
				require_relative modulev
			elsif cmd_first == 'extend_stdlib' then
				load $describescript_stdlib_dir + '/' + modulev + '.rb'
			else
				require modulev
			end
		rescue LoadError => e
			raise 'Module "' + modulev + '" is not installed, but it is required by these functions: ' + funcs.join(', ') + '(' + e.to_s + ')'
		end
	elsif cmd_first == 'in' then
		toask = cmd_split.join(' ').strip
		if not toask.empty? then
			print toask
			print ' '
			$stdout.flush
		end
		return STDIN.gets.gsub("\r\n", "\n").gsub("\n", "")
	elsif cmd_first == 'out_nonewline' then
		print cmd_split.join(' ').strip
		$stdout.flush
		return 0
	elsif cmd_first == 'ruby' then
		return eval(cmd_split.join(' ').strip).to_s
	elsif cmd_first == 'exit' then
		exit cmd_split.join('').to_i
	elsif cmd_first == 'm' then
		equation = cmd_split.join('')
		if not equation.is_maths? then
			return 0
		end
		return eval(equation).to_s
	elsif cmd_split.include?('+') || cmd_split.include?('&') then
		cmd_split.unshift(cmd_first_orig)
		sep = '+'
		if cmd_split.include?('&') then
			sep = '&'
		end
		before = functional_part(cmd_split.before(sep).join(' '))
		after = functional_part(cmd_split.after(sep).join(' '))
		return (before + after)
	elsif cmd_split.include?('==') || cmd_split.include?('!=') then
		cmd_split.unshift(cmd_first_orig)
		sep = '=='
		if cmd_split.include?('!=') then
			sep = '!='
		end
		before = cmd_split.before(sep)
		after = cmd_split.after(sep)
		truev = 0
		falsev = 1
		if sep == '!=' then
			truev = 1
			falsev = 0
		end
		#puts before.to_s
		#puts after.to_s
		if before.join(' ') == after.join(' ') then
			return truev
		end
		return falsev
	elsif $describescript_vars.keys.include? cmd_first_orig then
		return $describescript_vars[cmd_first_orig]
	elsif $describescript_extensions.include_key? 'func', cmd_first_orig then
		gen_query = cmd_first_orig + '('
		if cmd_split.length > 0 then
			cmd_split.each do |item|
				if item.to_i.to_s == item then
					gen_query += item
				else
					gen_query += '"' + item.gsub('"', '\"').gsub("\r", "\\r").gsub("\n", "\\n") + '"'
				end
				gen_query += ', '
			end
			gen_query = gen_query[0...-2].clone + ')'
		else
			gen_query += ')'
		end
		out = ''
		begin
			out = eval(subst_vars(gen_query, false))
		rescue StandardError => e
			puts 'Warning! An exception was caught while running "' + gen_query + '", so 1 will be returned for now.'
			puts e.to_s
			dbgputs 'backtrace = ' + e.backtrace.join("\n")
			out = 1
		end
		if Array === out then
			bag_out = 'BAG-'
			out.each do |item|
				bag_out += item.to_s.gsub('|', '[SPLITRP]') + '|'
			end
			bag_out = bag_out[0..-1]
			out = bag_out.clone
		end
		return out
	elsif $describescript_nativefunctions.include_key? 'name', cmd_first_orig then
		$describescript_vars['ds_func_returnedv'] = 0
		to_run = $describescript_nativefunctions.find_with_key('name', cmd_first_orig)['actions']
		dsrun(to_run)
		return $describescript_vars['ds_func_returnedv']
	elsif $describescript_extensions.include_key? 'func', possible_op then
		cmd_split.shift
		return functional_part(possible_op + ' "' + functional_part(cmd_first_orig).to_s + '" "' + functional_part(cmd_split.join(' ').strip).to_s + '"')
	else
		return (cmd_first_orig + ' ' + cmd_split.join(' ')).strip
	end
end

def stringify_hd(errc)
	num = errc.to_s
	if errc < 100 then
		if errc < 10 then
			num = '00' + num.clone
		else
			num = '0' + num.clone
		end
	end
	return num
end

def prettyerror(errc, line, text)
	real_line = if line < 0 then
			'[inline interpreter]'
		    else
			line.to_s
		    end
	puts 'On line ' + real_line + ' in "' + $describescript_vars['this'] + '":'
	interpreter_errc = 'DS' + stringify_hd(errc)
	puts interpreter_errc + '. ' + text
	exit errc
end

if ARGV.length < 1 || ARGV.include_any?(['--help', '-h', '-?']) then
	puts 'Usage: ruby "' + __FILE__ + '" <script>'
	exit 0
end


def dsrun(split_script)
	compared_var = nil
	current_line = 0
	log_lines = []
	various_info = []
	split_script.each do |row|
		current_line += 1
		if row.to_s[0] == '#' then
			next
		end
		rown_nosubst = row.gsub("\t", ' ').strip
		dbgputs "interational interpreter entered"
		rown = subst_commands(rown_nosubst)
		dbgputs "original line: #{rown_nosubst}"
		dbgputs "preprocessor converted line: #{rown}"
		row_split = rown.split_preserve_quotes(' ')
		row_first = row_split.shift.clone.to_s.downcase
		if row_first == 'switch' then
			compared_var = functional_part(row_split.join_ml(' ').strip)
		elsif row_first == 'case' then
			if row_split.length < 2 then
				prettyerror(5, current_line, 'Unfinished case (possible values missing).')
			end
			can_continue = false
			if not row_split.include? '=>' then
				prettyerror(6, current_line, 'Unfinished case (resulting action missing)')
			end
			items_shift = 1
			compare_next = false
			row_split.each do |item|
				if item.to_s == compared_var || item.to_s.downcase == 'other' then
					can_continue = true
				elsif item == '||' then
					sleep 0
				elsif item == '&&' then
					compare_next = true
				elsif compare_next then
					compare_next = false
					if item.to_s != compared_var.to_s && item.to_s.downcase != 'other' then
						can_continue = false
					end
				elsif item == '=>' then
					break
				end
				items_shift += 1
			end
			row_split.shift(items_shift)
			if can_continue then
				functional_part(row_split.join_ml(' '))
			end
		elsif row_first == 'block' || row_first == 'each' || row_first == 'below' || row_first == 'do' || row_first == 'then' || row_first == 'repeat' || (row_split[-1] == 'block' && row_split[-2] == '=>') then
			cline_real = current_line
			times_block_str = row_split.join(' ').to_s.strip
			times_block = times_block_str.gsub(' ', '').to_i
			listing_for_each = []
			restored_ctimes_value = ''
			each_index = -1
			if row_split[-1] == 'block' && row_split[-2] == '=>' then
				times_block = 1
				row_split.pop
				row_split.pop
				functional_part(row_first + ' ' + row_split.join_ml(' '))
			end
			if times_block == 0 then
				times_block = 1
			end
			if row_first == 'each' then
				out_times_block = functional_part(times_block_str)
				dbgputs "#{out_times_block} is what \"#{times_block_str}\" returned"
				listing_for_each = out_times_block.to_s.split('')
				if out_times_block.downcase.start_with? 'bag-' then
					listing_for_each = bagtoarray_dup(out_times_block)
				end
				times_block = listing_for_each.length
				if $describescript_vars.keys.include? 'current_item' then
					restored_ctimes_value = $describescript_vars['current_item'].clone
				end
			end
			snapshot_lines = split_script[cline_real..split_script.length - 1]
			log_lines = nil
			log_lines = []
			snapshot_lines.each do |line|
				log_lines.push(line.clone)
				split_script[cline_real] = '0'
				cline_real += 1
				#puts 'from do: ' + line.gsub("\t", ' ').strip
				if line.gsub("\t", ' ').strip.downcase == 'stop' || line.gsub("\t", ' ').strip.downcase == 'finish' then
					log_lines.pop
					if restored_ctimes_value != '' then
						log_lines.push('current_item = "' + restored_ctimes_value.to_s + '"')
					end
					break
				end
			end
			# puts 'split_script = ' + split_script.to_s
			if $describescript_allowblock then
				times_block.times do
					if listing_for_each.length > 0 then
						dbgputs 'each block'
						each_index += 1
						dbgputs "index #{each_index}"
						$describescript_vars['current_item'] = listing_for_each[each_index]
						dbgputs "current_item = #{$describescript_vars['current_item']}"
					end
					dbgputs "running this code:\n" + snapshot_lines.join("\n")
					dsrun(snapshot_lines)
				end
			elsif $describescript_currentlyfunction then
				$describescript_nativefunctions[-1]['actions'] = log_lines.clone
				$describescript_currentlyfunction = false
			end
			$describescript_allowblock = true
		elsif row_split.include? '=' then
			#if row_split.length < 2 then
			#	prettyerror(7, current_line, 'Missing arguments for = (left side: variable name, right side: variable value).')
			#end
			if row_split[0] != '=' then
				prettyerror(8, current_line, 'Variable name cannot contain spaces.')
			end
			if not $describescript_vars.keys.include? row_first then
				$describescript_vars = $describescript_vars.merge({ row_first => '' }).clone
			end
			row_split.shift
			$describescript_vars[row_first] = functional_part(row_split.join_ml(' ').gsub('#{me}', '#{' + row_first + '}'))
		else
			functional_part(rown_nosubst)
			next
		end
	end
end

arg_count = 0
script_to_run = nil
ARGV.each do |script|
	if File.exists?(script) && script_to_run == nil then
		script_to_run = script
		$describescript_vars.merge!({ 'this' => script_to_run.clone })
	elsif script.gsub('--', '-').downcase == '-with-gui' || script.gsub('--', '-').downcase == '-enable-gui' then
		$describescript_modulesdisabled.delete('tk')
	elsif script.gsub('--', '-').downcase.start_with?('-disable-module') && script.include?('=') then
		script_real = script.split('=')
		script_real.shift
		script_real = script_real.join(',')
		if script_real.include? ',' then
			script_real.split(',').each do |modulev|
				if not $describescript_modulesdisabled.include? modulev then
					$describescript_modulesdisabled.push(modulev)
				else
					puts 'Warning! "' + modulev + '" is already disabled.'
				end
			end
		else
			$describescript_modulesdisabled.push(script_real)
		end
		puts 'Warning! Per options specified on the command line, these modules are disabled: ' + $describescript_modulesdisabled.to_s
		puts '         Their commands will not be available unless you re-enable them.'
	else
		arg_count += 1
		$describescript_vars.merge!({ 'arg' + arg_count.to_s => script.to_s })
	end
end

if $0 == __FILE__ then
	if script_to_run == nil then
		puts 'No file specified to run.'
		exit 1
	end
end

stdlib_tarfile = File.dirname(File.absolute_path(__FILE__)) + '/stdlib.tar'
if File.exists?(Dir.pwd + '/stdlib.tar') then
	stdlib_tarfile = Dir.pwd + '/stdlib.tar'
end
if File.exists? stdlib_tarfile then
	Minitar.unpack(stdlib_tarfile, $describescript_stdlib_dir)
	regexp = $describescript_stdlib_dir + '/*.ds'
	Dir.glob(regexp).each do |file|
		begin
			if not $describescript_modulesdisabled.include? File.basename(file).split('.')[0].downcase then
				dsrun(File.read(file).gsub("\r\n", "\n").split("\n"))
			end
		rescue StandardError => e
			puts 'Warning! Module "' + File.basename(file) + '" could not be loaded from the standard library due to this error: ' + e.to_s
		end
	end
end

if $0 == __FILE__ then
	script_ctnt = File.read(script_to_run)
	split_script = script_ctnt.gsub("\r\n", "\n").split("\n")
	dsrun(split_script)
	FileUtils.rm_rf($describescript_stdlib_dir)
end
