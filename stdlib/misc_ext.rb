#!/usr/bin/env ruby
require_relative 'types_internal'
begin
	require_relative 'ftp_ext'
rescue LoadError
	def ls_from_ftp(host, dir)
		return 'BAG-'
	end
end
require 'time'

def adaptpath(path)
	return path.clone.gsub("\\", '/')
end


def dsop_contains(val1, val2)
	listing = val1.to_s
	if isbag(val1.to_s) then
		listing = bagtoarray(val1.to_s)
	end
	puts "listing = #{listing}, val2 = #{val2}"
	result_b = (listing.include?(val2.to_s))
	if result_b then
		return 0
	end
	return 1
end

def dsop_leftbig(val1, val2)
	if val1.to_s.to_i.to_s == val1.to_s && val2.to_s.to_i.to_s == val2.to_s then
		int_ret = if val1.to_i > val2.to_i then
								0
						  else
								1
						  end
		return int_ret
	elsif isbag(val1.to_s) && isbag(val2.to_s) then
		if bagtoarray(val1.to_s).length > bagtoarray(val2.to_s).length then
			return 0
		end
		return 1
	elsif isdate(val1.to_s) && isdate(val2.to_s) then
		if Time.strptime(val1.to_s, '%Y-%m-%d') > Time.strptime(val2.to_s, '%Y-%m-%d') then
			return 0
		end
		return 1
	elsif val1.to_s.length > val2.to_s.length then
		return 0
	end
	return 1
end

def dsop_leftsmall(val1, val2)
	return dsop_leftbig(val2, val1)
end

def dsop_equalsequals(var1, var2)
	if var1.to_s == var2.to_s then
		return 0
	end
	return 1
end

def dsop_notequals(var1, var2)
	if dsop_equals(var1, var2) == 0 then
		return 1
	end
	return 0
end

def dsop_leftsmallequals(val1, val2)
	value = (dsop_leftsmall(val1, val2) == 0 || val1.to_s == val2.to_s)
	if value then
		return 0
	end
	return 1
end

def dsop_leftbigequals(val1, val2)
	value = (dsop_leftbig(val1, val2) == 0 || val1.to_s == val2.to_s)
	if value then
		return 0
	end
	return 1
end

def from_specifictype(type, bag)
	numbers_filters = [ 'digits', 'numbers', 'integers', 'random' ]
	strings_filters = [ 'words', 'strings', 'letters', 'characters', 'dirs', 'folders', 'directories', 'files' ]
	dates_filters = [ 'dates' ]
	characters_filters = [ 'letters', 'characters' ]
	directory_filters = [ 'directories', 'folders', 'dirs' ]
	file_filters = [ 'files' ]
	if File.directory?(adaptpath(bag.join(''))) then
		dir_path = adaptpath(bag.join(''))
		items = Dir.glob(dir_path + '/*')
		files = []
		folders = []
		items.each do |item|
			if File.directory?(adaptpath(item)) then
				folders.push(adaptpath(item))
			else
				files.push(adaptpath(item))
			end
		end
		if file_filters.include? type.downcase then
			return files
		elsif directory_filters.include? type.downcase then
			return folders
		end
	elsif bag.join('').downcase.start_with? 'ftp://' then
		real_host = bag.join('')
		host_split = real_host.split('/')
		hostname = real_host.shift
		hostname = hostname[6...hostname.length - 1]
		directory = '/' + real_host.join('/')
		files = []
		directories = []
		ls_from_ftp(hostname, directory).each do |item|
			outbag = bagtoarray(ls_from_ftp(hostname, item))
			fn_beautiful = 'ftp://' + hostname + '/' + item
			if outbag.length == 1 && outbag[0] == item then
				files.push(fn_beautiful)
			else
				directories.push(fn_beautiful)
			end
		end
	end
	result = []
	if type.downcase == 'random' then
		return bag[rand(bag.length - 1)]
	end
	bag.each do |item|
		if numbers_filters.include?(type) && item.to_s.to_i.to_s == item.to_s then
			result.push(item.to_i)
		elsif strings_filters.include?(type) then
			if characters_filters.include?(type) then
				if String === item && item.length == 1 then
					result.push(item)
				end
			elsif directory_filters.include?(type) then
				if String === item && File.directory?(adaptpath(item)) then
					result.push(adaptpath(item))
				end
			elsif file_filters.include?(type) then
				if String === item && File.exists?(adaptpath(item)) && File.directory?(adaptpath(item)) == false then
					result.push(adaptpath(item))
				end
			else
				if String === item then
					result.push(item)
				end
			end
		elsif dates_filters.include?(type) then
			if isdate(item) then
				result.push(item.to_s)
			end
		end
	end
	return 'BAG-' + result.join('|')
end

def wait(seconds)
	sleep seconds
	return 0
end

def dsop_from(val1, val2)
	listing = val2.to_s.split('')
	if isbag(val2.to_s) then
		listing = bagtoarray(val2.to_s)
	end
	if String === val1 || val1 > listing.length then
		if [ 'digits', 'numbers', 'integers', 'letters', 'characters', 'words', 'strings', 'dates', 'random', 'files', 'folders', 'directories', 'dirs' ].include? val1.downcase then
			return from_specifictype(val1.downcase, listing)
		else
			return ''
		end
	end
	return listing[val1]
end

def dsop_without(bag, arg)
	listing = bag.to_s.split('')
	if isbag(bag.to_s) then
		listing = bagtoarray(bag)
	end
	if arg.to_s.to_i.to_s == arg.to_s then
		intarg = arg.to_s.to_i
		if intarg < 0 then
			intarg = listing.length - ((-1) * (intarg))
		end
		listing.delete_at(intarg)
	elsif isbag(arg.to_s) then
		listing = listing - bagtoarray(arg.to_s)
	elsif String === arg && String === bag && isbag(bag) == false then
		return bag.to_s.gsub(arg, '')
	else
		listing.delete(arg)
	end
	return 'BAG-' + listing.join('|')
end

def sh(*args)
	out = ''
	args.each do |item|
		out += ' \'' + item + '\''
	end
	return (system(out) ? 0 : 1)
end

def no(var)
	if var.start_with? '#{' then
		return 0
	end
	return 1
end

def global(var)
	if not ENV.keys.include? var then
		return ''
	end
	return ENV[var]
end

def env(var)
	return global(var)
end
