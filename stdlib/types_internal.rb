#!/usr/bin/env ruby

def isbag(bagobj)
	return (String === bagobj && bagobj.start_with?('BAG-'))
end

def bagtoarray(bagobj)
	if not isbag(bagobj) then
		return []
	end
	bagstr = bagobj[4..bagobj.length]
	if bagstr == nil then
		return []
	end
	out = []
	bagstr.split('|').each do |item|
		actual_item = item.gsub('[SPLITRP]', '|')
		if actual_item.to_i.to_s == actual_item then
			out.push(actual_item.to_i)
		else
			out.push(actual_item.to_s)
		end
	end
	return out
end

def isbagofdates(bagobj)
	if not isbag(bagobj) then
		return false
	end
	bagtoarray(bagobj).each do |item|
		if not isdate(item.to_s) then
			return false
		end
	end
	return true
end

def bagtodatesarray(bagobj)
	if not isbagofdates(bagobj) then
		return bagtoarray(bagobj)
	end
	out = []
	bagtoarray(bagobj).each do |item|
		out.push(Time.strptime(item, '%Y-%m-%d'))
	end
	return out
end

def isdate(string)
	begin
		Time.strptime(string, '%Y-%m-%d')
	rescue ArgumentError
		return false
	rescue
		return false
	end
	return true
end

def mergebags(*bags)
	out = 'BAG-'
	bags.each do |bag|
		if isbag(bag) then
			bagtoarray(bag).each do |item|
				out += item.to_s.gsub('|', '[SPLITRP]') + '|'
			end
		end
	end
	return out[0..-1]
end

