require 'exifr/jpeg'

def has_exif(filename)
	if File.exists?(adaptpath(filename)) && EXIFR::JPEG.new(adaptpath(filename)).exif? then
		return 0
	end
	return 1
end

def exif(filename)
	return has_exif(adaptpath(filename))
end

def date_captured(filename)
	if not has_exif(filename) == 0 then
		return '1970-01-01'
	end
	return EXIFR::JPEG.new(adaptpath(filename)).date_time.strftime('%Y-%m-%d')
end

def capture_date(filename)
	return date_captured(filename)
end

def camera_model(filename)
	if not has_exif(filename) == 0 then
		return ''
	end
	return EXIFR::JPEG.new(adaptpath(filename)).model
end

def camera_used(filename)
	return camera_model(adaptpath(filename))
end
