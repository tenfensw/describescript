#!/usr/bin/env ruby
require_relative 'files'
require 'json'

def read_json(filename)
	if not exists_n(filename) then
		return ''
	end
	ctnt = File.read(filename).gsub("\r\n", "\n")
	resulting_hash = JSON.parse(ctnt)
	out = 'BAG-JSON|'
	resulting_hash.keys.each do |hashkey|
		out += hashkey + 'v=' + resulting_hash[hashkey].to_s.gsub('|', '[SPLITRP]') + '|'
	end
	out = out[0..-1].clone
	return out
end

def value_of(key, bag)
	if not isbag(bag.to_s) then
		return ''
	end
	next_is_value = false
	arrayv = bagtoarray(bag.to_s)
	if arrayv.shift.to_s != 'JSON' then
		return ''
	end
	arrayv.each do |item|
		if item.to_s == key.to_s then
			next_is_value = true
		elsif next_is_value && item.to_s.start_with?('v=') then
			item_slice = item.to_s.gsub('[SPLITRP]', '|')
			return item_slice[2..item_slice.length - 1]
		end
	end
	return ''
end

def create_json(bag='')
	if isbag(bag.to_s) && bagtoarray(bag).shift == 'JSON' then
		return bag.clone
	end
	return 'BAG-JSON'
end

def set_value(key, value, bag)
	if not isbag(bag) then
		return bag
	end
	arrayv = bagtoarray(bag)
	if arrayv.shift != 'JSON' then
		return bag
	end
	cloned_json = create_json(bag)
	if value_of(key, cloned_json) == '' then
		return cloned_json + '|' + key + '|v=' + value.to_s
	else
		cloned_json = 'BAG-JSON|'
		eachitem = false
		next_is_mod = false
		arrayv.each do |item|
			if eachitem then
				cloned_json += 'v='
				if next_is_mod then
					cloned_json += value.to_s
				else
					cloned_json += item.to_s
				end
			else
				cloned_json += item.to_s
				if item.to_s == key.to_s then
					next_is_mod = true
				end
			end
			eachitem = (not eachitem)
			cloned_json += '|'
		end
		return cloned_json[0..-1]
	end
	return 'BAG-JSON'
end

def new_json(bag='')
	return create_json(bag)
end

def write_json(bag, filename)
	if not isbag(bag.to_s) then
		if isbag(filename.to_s) then
			return write_json(filename.to_s, bag.to_s)
		else
			return 1
		end
	end
	arrayv = bagtoarray(bag.to_s)
	if arrayv.shift != 'JSON' then
		return 1
	end
	res_hash = {}
	last_key = nil
	arrayv.each do |item|
		if item.start_with? 'v=' then
			res_hash[last_key] = item[2..item.length - 1]
		else
			last_key = item.clone
			res_hash[last_key] = ''
		end
	end
	begin
		File.write(adaptpath(filename.to_s), JSON.generate(res_hash))
	rescue
		return 1
	end
	return 0
end
