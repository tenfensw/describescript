require_relative 'types_internal'

def c_define(filename, definename, definevalue=nil)
	if isbag(filename) || File.exists?(adaptpath(filename)) == false then
		return 'NULL'
	end
	ctnt = File.read(adaptpath(filename)).gsub("\r\n", "\n").split("\n")
	out = ''
	noappend = false
	ctnt.each do |item|
		itemv = item.gsub("\t", '').strip.split(' ')
		if itemv[0] == '#define' && itemv[1] == definename then
			if definevalue == nil then
				itemv.shift
				itemv.shift
				value = itemv.join(' ')
				if value.start_with? '"' then
					value = value[1..value.length - 1]
				elsif value.start_with? '\'' then
					value = value[1].clone
				end
				return value
			else
				noappend = true
				out += "\n#define " + definename + " "
				if definevalue.to_s.to_i.to_s == definevalue.to_s then
					out += definevalue.to_s
				else
					out += '"' + definevalue.to_s + '"'
				end
			end
		end
		if noappend then
			noappend = false
		else
			out += "\n" + item
		end
	end
	if definevalue == nil then
		return 'NULL'
	end
	File.write(adaptpath(filename), out)
	return 0
end

