#!/usr/bin/env ruby
require 'net/http'
require 'socket'
require 'uri'

class String
	def gsub_multi(hash)
		out = self
		hash.keys.each do |key|
			out = out.gsub(key, hash[key].to_s).clone
		end
		return out
	end
end

def read_from_url(url)
	resp = Net::HTTP.get_response(URI.parse(url))
	if resp.code.to_i != 200 then
		return ''
	end
	return resp.body
end

def curl(url)
	return read_from_url(url)
end

def read_url(url)
	return read_from_url(url)
end

def download(url, file)
	begin
		opened_uri = read_from_url(url)
		final_file = open(adaptpath(file), 'wb')
		final_file.write(opened_uri)
		final_file.close
		return 0
	rescue
		return 1
	end
end

def cgi_form_fields
	if ENV['QUERY_STRING'] == nil then
		return 'BAG-'
	end
	out = 'BAG-'
	ENV['QUERY_STRING'].to_s.split('&').each do |item|
		out += item.to_s.split('=')[0] + '|'
	end
	out = out[0..-1]
	return out.clone
end

def propertags(string)
	return string.gsub_multi({ '%3C' => '<', '%3E' => '>',
														 '+' => ' ', '%2B' => '+',
														 '%2F' => '/', '%5C' => "\\",
														 '%21' => '!', '%40' => '@',
														 '%23' => '#', '%24' => '$',
														 '%25' => '%', '%5E' => '^',
													 	 '%26' => '&', '%28' => '(',
														 '%29' => ')', '%5B' => '[',
														 '%5D' => ']', '%7B' => '{',
														 '%7D' => '}', '%3A' => ':',
														 '%3B' => ';', '%22' => '"',
														 '%27' => '\'', '%7C' => '|',
														 '%3D' => '=', '%3F' => '?',
														 '%20' => ' ' })
end

def cgi_field_value(field)
	if ENV['QUERY_STRING'] == nil then
		return ''
	end
	ENV['QUERY_STRING'].to_s.split('&').each do |item|
		split_item = item.to_s.split('=')
		if split_item.shift.to_s == field.to_s then
			return propertags(split_item.join('=')).clone
		end
	end
	return ''
end

def cgi_redirect(final_url)
	puts 'Content-type: text/html'
	puts ''
	puts '<html><head><title>Redirection notice</title>'
	puts '<meta http-equiv="refresh" content="0; url=' + final_url + '">'
	puts '</head><body><p>This page redirects to ' + final_url + '</p>'
	puts '<p>If you are not getting redirected, click <a href="' + final_url + '">'
	puts 'here</a></p></body></html>'
end

def cgi_error(code)
	exit 1
end

def cgi_serve(pagev, content_type="text/html")
	page = adaptpath(pagev)
	templates_path = Dir.pwd + '/../templates/' + page
	if File.exists? templates_path then
		page = templates_path
	end
	puts 'Date: ' + Time.now.to_s
	puts 'Content-type: ' + content_type
	puts 'X-DescribeScript-CGIModuleVersion: 0.2'
	puts 'Connection: closed'
	puts ''
	if not File.exists? page.to_s then
		puts ''
		puts '<h2 style="color: red">DescribeScript CGI serving error</h2>'
		puts '<p>The template or file "' + page + '" could not be served correctly, because it does not exist. If you are the developer of this web application, please fix the error by specifying the path to the file explicitly.</p>'
		puts '<p><b>Current server directory:</b> ' + Dir.pwd + '</p>'
		return 1
	end
	ctnt = File.read(page)
	if $describescript_vars != nil then
		$describescript_vars.keys.each do |key|
			ctnt = ctnt.gsub('#{' + key.to_s + '}', $describescript_vars[key].to_s).clone
		end
	end
	puts ctnt
	return 0
end

def cgi_accept_upload(output_filename)
	if ENV['REQUEST_METHOD'] == 'POST' && ENV['CONTENT_LENGTH'].to_s.to_i > 0 then
		begin
			file_obj = open(output_filename, 'wb')
			file_obj.write(ARGF.read)
			file_obj.close
			return 0
		rescue
			return 2
		end
	end
	return 1
end

def listen(port=8080, eventhandler='out "Incoming connection"')
	server = TCPServer.new(port)
	loop do
		Thread.start(server.accept) do |cl|
			ctnt = ''
			while line = cl.gets do
				ctnt += line + "\n"
			end
			$describescript_vars['request'] = ctnt
			$describescript_vars['response'] = 'nil'
			dsrun(eventhandler)
			client.puts $describescript_vars['response'].to_s
			client.close
		end
	end
	return 0
end
