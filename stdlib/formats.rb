require_relative 'types_internal'

def declare_format(fmt)
	if fmt.to_s.downcase.include?('[key]') == false || fmt.to_s.downcase.include?('[value]') == false then
		return ''
	end
	if isbag(fmt) then
		out = ''
		bagtoarray(fmt).each do |item|
			if out == '' then
				out = item.to_s.gsub('[SPLITRP]', '|')
			else
				out += "\n" + item.to_s.gsub('[SPLITRP]', '|')
			end
			out = out.gsub('[Key]', '[key]').gsub('[Value]', '[value]')
		end
		return out
	end
	return fmt.to_s.clone.gsub('[Key]', '[key]').gsub('[Value]', '[value]').gsub('[ID]', '[Id]').gsub('[Id]', '[id]')
end

def create_format(fmt)
	return declare_format(fmt)
end

def new_format(fmt)
	return declare_format(fmt)
end

def follow_format(fmt, key, value, *others)
	if isbag(fmt) then
		return ''
	end
	fmt_real = fmt.to_s.clone
	fmt_real = fmt_real.gsub('[key]', key.to_s).gsub('[value]', value.to_s).clone
	others.each do |item|
		if item.to_s.include? '=' then
			halfsplit = item.to_s.split(':')
			halfone = halfsplit.shift
			halftwo = halfsplit.join('=')
			fmt_real = fmt_real.gsub('[' + halfone + ']', halftwo).gsub('[' + halfone.downcase + ']', halftwo).clone
		end
	end
	return fmt_real
end

def format(fmt, key, value, *others)
	return follow_format(fmt, key, value, others)
end

def getallkindsofkeyindexes(arg)
	write_key = false
	index = -1
	start_index = 0
	keyname = ''
	out = []
	arg.split('').each do |keychar|
		index += 1
		if keychar == '[' then
			write_key = true
			start_index = index.clone
		elsif keychar == ']' then
			write_key = false
			out.push({ 'start' => start_index.clone, 'end' => index.clone,
				 				 'name' => keyname.clone })
			keyname = ''
		elsif write_key then
			keyname += keychar
		end
	end
	return out
end

def from_format(fmt, string, key='[key]')
	if isbag(fmt) then
		return ''
	end
	if not key.start_with? '[' then
		return from_format(fmt, string, '[' + key + ']')
	end
	fmt_orig = fmt
	hash_wm = { '[ds_format_string]' => fmt }
	fmt_indexes = getallkindsofkeyindexes(fmt_orig)
	dbgputs "fmt_indexes = #{fmt_indexes}"
	offset = 0
	index = -1
	current_fmt_index = 0
	string.split('').each do |clone|
		index += 1
		dbgputs "index = #{index}"
		hash_index = fmt_indexes[current_fmt_index] || { 'start' => -1, 'end' => -1, 'name' => nil }
		dbgputs "hash_index = #{hash_index}"
		start_index_o = hash_index['start'] + offset
		end_index_o = hash_index['end'] + offset
		dbgputs "start at #{start_index_o}, end at #{end_index_o} (offset = #{offset}"
		if index == start_index_o then
			dbgputs "if entered"
			ending_character = fmt_orig[hash_index['end'] + 1] || ''
			dbgputs "end char #{ending_character}"
			local_index = index.clone
			val_flush = ''
			while string[local_index] != ending_character do
				if local_index < string.length then
					val_flush += string[local_index]
				else
					break
				end
				local_index += 1
			end
			#val_flush = val_flush[0...-1]
			dbgputs "#{val_flush}"
			offset = offset + (local_index - hash_index['end']) - 1
			#if offset < 0 then
			#	offset = 0
			#end
			naming = '[' + hash_index['name'].to_s + ']'
			dbgputs "naming = #{naming}, new_offset = #{offset}"
			hash_wm[naming] = val_flush.clone
			current_fmt_index += 1
		end
	end
	if not hash_wm.keys.include? key then
		return ''
	end
	return hash_wm[key]
end
