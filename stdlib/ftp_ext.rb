#!/usr/bin/env ruby
require 'net/ftp'
require_relative 'types_internal'

def createftpobj(host)
	if isbag(host) || Integer === host then
		return nil
	end
	host_orig = host.clone
	user = nil
	password = nil
	if host.include? '@' then
		host_split = host.split('@')
		user = host_split.shift
		if user.include? ':' then
			password = user.split(':')[1]
			user = user.split(':')[0]
		end
		host = host_split.join('@')
	end
	ftp = Net::FTP.new(host)
	if user != nil then
		ftp.login(user, password)
	else
		ftp.login
	end
	return ftp
end

def ls_from_ftp(host, dir)
	ftp = createftpobj(host)
	if ftp == nil then
		return 'BAG-'
	end
	out = 'BAG-'
	ftp.nlst(dir).each do |line|
		out += line.gsub('|', '[SPLITRP]') + '|'
	end
	out = out[0...-1]
	ftp.close
	return out
end
