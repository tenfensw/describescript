#!/usr/bin/env ruby
require_relative 'types_internal'

def length(string)
  if isbag(string.to_s) then
    return bagtoarray(string.to_s).length
  else
    return string.to_s.length
  end
end

def sort(string)
  if string.to_s.to_i.to_s == string.to_s then
    return string.to_s.to_i
  elsif isbag(string.to_s) then
    arraybag = bagtoarray(string)
    
    return 'BAG-' + arraybag.sort.join('|')
  else
    return string.split('').sort.join('')
  end
end

def reverse(string)
  if isbag(string.to_s) then
    return 'BAG-' + bagtoarray(string).reverse.join('|')
  else
    return string.to_s.reverse
  end
end

def split(string, sep)
  out = 'BAG-'
  first = true
  string.split(sep.gsub("\\n", "\n")).each do |word|
    if first then
      first = false
    else
      out += '|'
    end
    out += word.gsub('|', '[SPLITRP]')
  end
  return out
end

def concat(val1, val2, val3='', val4='', val5='', val6='', val7='', val8='', val9='')
  listing = [ val1, val2, val3, val4, val5, val6, val7, val8, val9 ]
  first_val = listing[0].clone
  if isbag(first_val) then
    first_val = bagtoarray(val1)
  end
  listing.each do |item|
    if Array === first_val then
      first_val.push(item)
    else
      first_val += item.to_s
    end
  end
  if Array === first_val then
    return 'BAG-' + first_val.join('|')
  end
  return first_val.clone
end

def split_into_words(string)
  return split(string, ' ')
end

def split_into_lines(string)
  return split(string, "\n")
end

def sum(bag)
  sum_out = 0
  if not isbag(bag.to_s) then
    bag.to_s.split('').each do |item|
      if item.to_i.to_s == item then
        sum_out += item.to_i
      end
    end
  else
    bagtoarray(bag).each do |item|
      if item.to_s.to_i.to_s == item.to_s then
        sum_out += item.to_s.to_i
      end
    end
  end
  return sum_out
end

def mean(bag)
  if not isbag(bag.to_s) then
    return (sum(bag.to_s) / bag.to_s.length)
  else
    return (sum(bag.to_s) / length(bag))
  end
end

def remove_punctuation(string)
  if not isbag(string.to_s) then
    return string.to_s.gsub('!', '').gsub('?', '').gsub('.', '').gsub(',', '').gsub('-', '').gsub('(', '').gsub(')', '').clone
  else
    input = bagtoarray(string).join('|')
    removed_punct = remove_punctuation(input)
    return split(removed_punct, '|')
  end
end

def join(bag, sep=' ')
  if not isbag(bag) then
    return bag
  end
  return bagtoarray(bag).join(sep.gsub("\\n", "\n"))
end

def empty(bag='')
  if isbag(bag.to_s) then
    if bagtoarray(bag).length < 1 then
      return 0
    end
  elsif bag.to_s.to_i.to_s == bag.to_s then
    if bag.to_s.to_i == 0 then
      return 0
    end
  else
    if bag.length < 1 then
      return 0
    end
  end
  return 1
end

def merge(*bags)
	return mergebags(bags)
end
