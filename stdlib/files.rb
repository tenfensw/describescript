require 'fileutils'
require 'time'
require 'digest'
require_relative 'types_internal'

def exists_n(filename)
	return File.exists?(adaptpath(filename))
end

def exists(filename)
	return (exists_n(filename) ? 0 : 1)
end

def new_file(filename)
	if exists_n(filename) then
		return 1
	end
	File.write(adaptpath(filename), '')
	return 0
end

def touch(filename)
	return new_file(filename)
end

def new_dir(filename)
	if exists_n(filename) then
		return 1
	end
	FileUtils.mkdir_p(filename)
	return 0
end

def new_folder(filename)
	return new_dir(filename)
end

def mkdir(filename)
	return new_dir(filename)
end

def delete(filename)
	if not exists_n(filename) then
		return 1
	end
	FileUtils.rm_rf(adaptpath(filename))
	return 0
end

def rm(filename)
	return delete(filename)
end

def append_to_file(filename, ctnt)
	if not exists_n(filename) then
		return 1
	end
	orig_ctnt = ""
	sep = "\n"
	if ENV['OS'] == 'Windows_NT' then
		sep = "\r\n"
	end
	begin
		file_tmpfile = open(adaptpath(filename), 'r')
		orig_ctnt = file_tmpfile.read
		file_tmpfile.close
		if orig_ctnt.length >= 2 then
			orig_ctnt += sep
		end
		if isbag(ctnt.to_s) then
			orig_ctnt += bagtoarray(ctnt.to_s).join(sep)
		else
			orig_ctnt += ctnt.to_s
		end
		file_tmpfile = nil
		file_tmpfile = open(adaptpath(filename), 'w')
		file_tmpfile.write(orig_ctnt.gsub("\\n", sep))
		file_tmpfile.close
	rescue
		return 1
	end
	return 0
end

def read_file(filename)
	if not exists_n(filename) || File.directory?(adaptpath(filename)) then
		return ''
	end
	return File.read(adaptpath(filename)).gsub("\r\n", "\n")
end

def read_from_file(filename)
	return read_file(filename)
end

def cat(filename)
	return read_file(filename)
end

def rename_single(orig, dest)
	if exists_n(orig) == false then
		return 1
	end
	begin
		FileUtils.mv(adaptpath(orig), adaptpath(dest))
	rescue
		return 1
	end
	return 0
end

def rename(orig, dest)
	if not isbag(orig.to_s) then
		return rename_single(orig, dest)
	end
	listing_files = bagtoarray(orig.to_s)
	listing_files.each do |file|
		if rename_single(adaptpath(file), adaptpath(dest)) != 0 then
			return 1
		end
	end
	return 0
end

def move(orig, dest)
	return rename(orig, dest)
end

def mv(orig, dest)
	return rename(orig, dest)
end

def date_modified(file)
	if not exists_n(file) then
		return '1970-01-01'
	end
	return File.mtime(adaptpath(file)).strftime('%Y-%m-%d')
end

def basename(file)
	return File.basename(adaptpath(file))
end

def dirname(file)
	return File.dirname(File.absolute_path(adaptpath(file)))
end

def filename(file)
	return basename(file)
end

def parent_dir(file)
	return dirname(file)
end

def copy(file_in, file_out)
	if isbag(file_in) then
		if not File.directory?(adaptpath(file_out)) then
			return 1
		end
		bagtoarray(file_in).each do |file|
			out_path = adaptpath(file_out) + '/' + File.basename(file)
			if copy(adaptpath(file), out_path) != 0 then
				return 2
			end
		end
	else
		if not exists_n(file_in) then
			puts "#{file_in} does not exist, returning 1"
			return 1
		end

		FileUtils.cp(adaptpath(file_in), adaptpath(file_out))
	end
	return 0
end

def cp(var1, var2)
	return copy(var1, var2)
end

def chmod(file, mode=755)
	if not exists_n(file) then
		return 1
	end
	begin
		FileUtils.chmod(mode, adaptpath(file))
	rescue
		return 1
	end
	return 0
end

def make_executable(file)
	return chmod(file, 0775)
end

def sort_by_date_modified(files_bag)
	if not isbag(files_bag) then
		return 'BAG-'
	end
	out = 'BAG-'
	bagtoarray(files_bag).sort_by {|it| File.mtime(adaptpath(it))}.each do |item|
		out += adaptpath(item).gsub('|', '[SPLITRP]') + '|'
	end
	return out[0...-1].clone
end

def sort_by_date(files_bag)
	return sort_by_date_modified(files_bag)
end

def same_file(file1, file2)
	if isbag(file2) then
		return 3
	elsif isbag(file1) then
		bagtoarray(file1).each do |item|
			if same_file(adaptpath(item), adaptpath(file2)) != 0 then
				return 4
			end
		end
		return 0
	elsif exists_n(file1) == false || exists_n(file2) == false then
		return 2
	end
	if Digest::SHA256.hexdigest(File.read(adaptpath(file1))) == Digest::SHA256.hexdigest(File.read(adaptpath(file2))) then
		return 0
	end
	return 1
end

def same_files(*files)
	prev = nil
	files.each do |item|
		if prev == nil then
			prev = item
			next
		end
		if same_file(prev, item) != 0 then
			return 1
		end
		prev = item
	end
	return 0
end

def find(patterns)
	out = 'BAG-'
	if isbag(patterns) then
		bagtoarray(patterns).each do |item|
			out = mergebags(out, find(item))
		end
	else
		Dir.glob(patterns).each do |item|
			out += adaptpath(item).to_s.gsub('|', '[SPLITRP]') + '|'
		end
		out = out[0..-1]
	end
	return out
end

def search(patterns)
	return find(patterns)
end
