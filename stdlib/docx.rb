require 'nokogiri'
require 'zip'
require_relative 'types_internal'

# pretty much a Ruby adaptation of this project:
# https://github.com/ankushshah89/python-docx2txt/blob/master/docx2txt/docx2txt.py

def docx_tags_to_text(tag)
	text = ''
	xmldoc = Nokogiri::XML(tag)
	xmldoc.xpath('//*').each do |xtag|
		if xtag.name == 't' then
			text += xtag.content.strip.to_s
		elsif xtag.name == 'tab' then
			text += "\t"
		elsif ['br', 'cr'].include? xtag.name then
			text += "\n"
		elsif xtag.name == 'p' then
			text += "\n\n"
		end
	end
	return text
end

def libreoffice_tags_to_text(tag)
	xmldoc = Nokogiri::XML(tag)
	paragraphs = ''
	xmldoc.xpath('//*').each do |xtag|
		if xtag.name == 'p' then
			paragraphs += xtag.content.strip.to_s + "\n"
		end
	end
	return paragraphs
end

def read_docx(filename)
	if isbag(filename) then
		out = ''
		bagtoarray(filename).each do |fn|
			if File.exists? fn then
				out += read_docx(adaptpath(fn.to_s)) + "\n"
			end
		end
		return out
	end
	out = ''
	Zip::File.open(adaptpath(filename)) do |zf|
		entry = zf.glob('word/document.xml').first
		firstread = entry.get_input_stream.read
		out = docx_tags_to_text(firstread)
	end
	return out
end

def read_odt(filename)
	if isbag(filename) then
		out = ''
		bagtoarray(filename).each do |fn|
			if File.exists? fn then
				out += read_odt(adaptpath(fn.to_s)) + "\n"
			end
		end
		return out
	end
	out = ''
	Zip::File.open(adaptpath(filename)) do |zf|
		entry = zf.glob('content.xml').first
		firstread = entry.get_input_stream.read
		out = libreoffice_tags_to_text(firstread)
	end
	return out
end

def read_from_odt(filename)
	return read_odt(filename)
end

def read_from_docx(filename)
	return read_docx(filename)
end
