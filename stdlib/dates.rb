#!/usr/bin/env ruby
require 'time'
require_relative 'types_internal'

def today
  return Time.now.strftime('%Y-%m-%d')
end

def weekday(date)
  if not isdate(date) then
    return ''
  end
  weekday_int = Time.strptime(date, '%Y-%m-%d').wday
  weekdays_set = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ]
  return weekdays_set[weekday_int].to_s
end

def next_day(date)
  if not isdate(date) then
    return '1970-01-01'
  end
  day = Time.strptime(date, '%Y-%m-%d')
  return (day + (60 * 60 * 24)).strftime('%Y-%m-%d')
end

def previous_day(date)
  if not isdate(date) then
    return '1970-01-01'
  end
  day = Time.strptime(date, '%Y-%m-%d')
  return (day - (60 * 60 * 24)).strftime('%Y-%m-%d')
end

def yesterday
  return previous_day(today)
end

def tomorrow
  return next_day(today)
end

def week(day)
  if not isdate(day) then
    return 'BAG-'
  end
  dayconv = Time.strptime(day, '%Y-%m-%d')
  while dayconv.wday != 0 do
    dayconv = previous_day(dayconv).clone
  end
  out = 'BAG-' + dayconv.strftime('%Y-%m-%d')
  6.times do
    dayconv = next_day(dayconv)
    out += '|' + dayconv.strftime('%Y-%m-%d')
  end
  return out
end

def day_of_year(day)
  if not isdate(day) then
    return 1
  end
  dayconv = Time.strptime(day, '%Y-%m-%d')
  return dayconv.yday
end
