#!/usr/bin/env ruby
require 'tk'
$dstk_windows = []
$dstk_handlers = []
$dstk_windowpos = []
$dstk_windowautosizing = []

def filteredresizing(id)
	if not validwindow(id) then
		return 1
	end
	window_real = $dstk_windows[id]
	$dstk_windowautosizing.each do |item|
		if item['offset_parent'] == id then
			item_r = $dstk_windows[item['widget']]
			if item_r != nil then
				item_r.width = window_real.width - item['offsets']['right'] + item['offsets']['left']
			end
		end
	end
	return 0
end
 
def gui_openwindow(title='DescribeScript+TK')
	$dstk_windows.push(TkRoot.new { title title })
	$dstk_windowpos.push({ 'x' => 0, 'y' => 0 })
	$dstk_windows[-1].menu(TkMenu.new)
	$dstk_windows[-1].bind('<Configure>', proc { filteredresizing($dstk_windows.length - 1) })
	return ($dstk_windows.length - 1)
end

def gui_infomessage(message, title='Information')
	Tk.messageBox({ 'type' => 'ok', 'icon' => 'info',
			'title' => title, 'message' => message })
	return 0
end

def gui_errormessage(message, title='Error')
	Tk.messageBox({ 'type' => 'ok', 'icon' => 'error',
			'title' => title, 'message' => message })
	return 0
end

def gui_yesnomessage(message, title='Question')
	btn = Tk.messageBox({ 'type' => 'yesno', 'icon' => 'question',
			'message' => message, 'title' => title })
	if btn == 'yes' then
		return 0
	end
	return 1
end

def gui_newwindow(title='DescribeScript+TK')
	return gui_openwindow(title)
end

def finddstkhandler(window)
	$dstk_handlers.each do |item|
		if Hash === item && item['associated_widget'] == window then
			return item
		end
	end
	return nil
end

def validwindow(window_id)
	if window_id.to_s.to_i.to_s != window_id.to_s || $dstk_windows.length <= window_id || $dstk_windows[window_id] == nil then
		return false
	end
	return true
end

def rundstkrunner(window)
	hash = finddstkhandler(window)
	puts 'hash is ' + hash.to_s
	if hash == nil then
		puts 'hash is actually nil'
		return
	end
	puts 'about to run ' + hash['runner']
	functional_part(hash['runner'])
end

def gui_newlabel(window, text='Label', coordsx=0, coordsy=0, vwidth=-1, vheight=-1)
	if not validwindow(window) then
		return false
	end
	window_real = $dstk_windows[window]
	label = TkLabel.new(window_real) do
		text text
		if vwidth >= 0 && vheight >= 0 then
			width vwidth
			height vheight
		end
		place('x' => coordsx, 'y' => coordsy)
	end
	$dstk_windows.push(label)
	$dstk_windowpos.push({'x' => coordsx, 'y' => coordsy})
	return $dstk_windows.length - 1
end

def gui_newbutton(window, text='Button', coordsx=0, coordsy=0, vwidth=-1, vheight=-1)
	if not validwindow(window) then
		return false
	end
	button = TkButton.new($dstk_windows[window]) do
		text text
		if vwidth >= 0 && vheight >= 0 then
			width vwidth
			height vheight
		end
		place('x' => coordsx, 'y' => coordsy)
	end
	$dstk_windows.push(button)
	$dstk_windowpos.push({'x' => coordsx, 'y' => coordsy})
	return $dstk_windows.length - 1
end

def gui_finish
	Tk.mainloop
end

def gui_changetextcolor(window, colorstr)
	if not validwindow(window) then
		return 1
	end
	$dstk_windows[window].configure('foreground', colorstr.to_s)
	return 0
end

def gui_settextcolor(window, colorstr)	
	return gui_changetextcolor(window, colorstr)
end

def gui_changecolor(window, colorstr)
	if not validwindow(window) then
		return 1
	end
	$dstk_windows[window].configure('activebackground', colorstr.to_s)
	return 0
end

def gui_setcolor(window, colorstr)
	return gui_changecolor(window, colorstr)
end

def gui_changetext(window, text)
	if not validwindow(window) then
		return 1
	end
	$dstk_windows[window].text text.to_s
	return 0
end

def gui_settext(window, text)
	return gui_changetext(window, text)
end


def gui_newdocumentarea(window, coordsx=0, coordsy=0, vwidth=-1, vheight=-1)
	if not validwindow(window) then
		return 1
	end
	text = TkText.new($dstk_windows[window]) do
		if vwidth >= 0 && vheight >= 0 then
			width vwidth
			height vheight
		end
		place('x' => coordsx, 'y' => coordsy)
	end
	$dstk_windowpos.push({'x' => coordsx, 'y' => coordsy})
	$dstk_windows.push(text)
	return text
end

def gui_runwhentriggered(window, runner)
	puts 'triggered'
	if not validwindow(window) then
		puts 'not a vailid window'
		return 1
	end
	puts 'valid window'
	$dstk_handlers.push({ 'associated_widget' => window, 'runner' => runner.to_s })
	$dstk_windows[window].command = proc { rundstkrunner(window) }
	return 0
end

def gui_setimage(window, filepath)
	if not File.exists? filepath then
		return 1
	elsif not validwindow(window) then
		return 2
	end
	image = TkPhotoImage.new
	image.file = filepath
	$dstk_windows[window].image = image
	return 0
end

def gui_changeimage(window, filepath)
	return gui_setimage(window, filepath)
end

def gui_resize(window, width, height)
	if not validwindow(window) then
		return 1
	end
	$dstk_windows[window].width = width
	$dstk_windows[window].height = height
	return 0
end

def gui_setsize(window, width, height)
	return gui_resize(window, width, height)
end

def gui_changesize(window, width, height)
	return gui_resize(window, width, height)
end

def next_to(window)
	if not validwindow(window) then
		return 0
	end
	return $dstk_windowpos[window]['x'] + $dstk_windows[window].cget('width') + 3
end

def under(window)
	if not validwindow(window) then
		return 0
	end
	return $dstk_windowpos[window]['y'] + $dstk_windows[window].cget('height') + 3
end

def gui_dynamicallyresize(parentwindow, window, offset_left, offset_right=-1)
	if validwindow(window) == false || validwindow(parentwindow) == false then
		return 1
	end
	offset_right_real = offset_right
	if offset_right_real < 0 then
		offset_right_real = offset_left
	end
	$dstk_windowautosizing.push({ 'offset_parent' => parentwindow,
				      'widget' => window,
				      'offsets' => { 'left' => offset_left,
						     'right' => offset_right_real }
				    })
end

def gui_newmenu(window, label, *itemsv)
	if not validwindow(window) == false then
		return 1
	end
	menu = TkMenu.new($dstk_windowpos[window])
	oddv = false
	labelv = 'Item'
	commandv = nil
	items = itemsv.clone
	items.delete('runs')
	items.each do |iter|
		if iter == 'seperator' || iter == 'line' then
			oddv = false
			menu.add('separator')
			next
		end
		oddv = (not oddv)
		if oddv then
			labelv = iter
		else
			commandv = proc { dsrun([ iter ]) }
			menu.add('command', 'label' => labelv, 'command' => commandv)
		end
	end
	menu_window = window_real.cget('menu').clone
	menu_window.add('cascade', 'menu' => menu,
				   'label' => label)
	$dstk_windowpos[window].menu(menu_window)
	return 0
end

def gui_choosefile
	return Tk.getOpenFile
end

def gui_choosedirectory
	return Tk.chooseDirectory
end

def gui_choosefolder
	return gui_choosedirectory
end

def gui_gettext(obj)
	if not validwindow(obj) then
		return ''
	end
	return $dstk_windows[obj].text.to_s
end

def gui_textof(obj)
	return gui_gettext(obj)
end
