#!/bin/sh
printf "extend_stdlib from `basename "$1" .rb` " > "$2"
echo `cat "$1" | grep "def" | grep '(' | cut -d '(' -f1 | cut -d ' ' -f2-` >> "$2"
exit $?
