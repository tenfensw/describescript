#!/usr/bin/env ruby
require 'minitar'

output = Dir.pwd + '/stdlib.tar'
if File.exists? output then
	File.delete(output)
end

Dir.chdir Dir.pwd + '/stdlib'
actual_files = []
Dir.glob(Dir.pwd + '/*').each do |file|
	if system('"' + RbConfig.ruby + '" -c "' + file + '"') == false then
		puts 'Syntax errors in "' + file + '", cannot continue.'
		exit 1
	end
	actual_files.push(File.basename(file))
end

Minitar.pack(actual_files, File.open(output, 'wb'))
puts 'Wrote ' + output
exit 0
