#!/bin/sh
set -e
PREFIX="/opt/describescript"
RUBYV="https://cache.ruby-lang.org/pub/ruby/2.4/ruby-2.4.6.tar.gz"
OPENSSLV="https://www.openssl.org/source/openssl-1.0.2s.tar.gz"
DESCRIBESCRIPTV="https://gitlab.com/timkoi/describescript/-/archive/master/describescript-master.tar"

sudo rm -r -f build
mkdir -p build/openssl
wget -O- "$OPENSSLV" | tar --strip-components=1 -C build/openssl -xvzf -

OLDPWD="$PWD"
cd build/openssl
if echo "$*" | grep -q "\-no-rebuild-openssl"
then
	echo "Skip OpenSSL"
else
	./Configure no-dso no-shared no-zlib no-asm --prefix="$PREFIX" linux-x86_64
	make -j4
	sudo make install
fi

cd ../
mkdir ruby
wget -O- "$RUBYV" | tar -C ruby --strip-components=1 -xvzf -
cd ruby
export PKG_CONFIG_PATH="$PREFIX/lib/pkgconfig"
if echo "$*" | grep -q "\-no-rebuild-ruby"
then
	echo "Skip Ruby rebuilding"
else
	./configure --prefix="$PREFIX" --build=x86_64-ubuntu-linux-gnu --without-gmp --without-git --disable-install-rdoc
	make -j4
	sudo make install
fi
cd ..
sudo "$PREFIX/bin/gem" install -N minitar nokogiri exifr
rm -r -f *
sudo rm -r -f -v "$PREFIX/share" "$PREFIX/lib/libruby-static.a" "$PREFIX/lib/pkgconfig" "$PREFIX/include"
mkdir describescript
wget -O- "$DESCRIBESCRIPTV" | tar -C describescript --strip-components=1 -xvf -
sudo mkdir -p "$PREFIX/libexec/describescript"
cd describescript
"$PREFIX/bin/ruby" build-stdlib.rb
sudo mv -v stdlib.tar "$PREFIX/libexec/describescript/stdlib.tar"
mv -v describescript.rb describescript.rb.y
touch describescript.rb
printf '#!' > describescript.rb
echo "$PREFIX/bin/ruby" >> describescript.rb
cat describescript.rb.y | tail -n +2 >> describescript.rb
rm -r -f -v describescript.rb.y
export PATH="$PREFIX/bin:$PATH"
export PREFIX
sudo sh ./install/bootstrap.sh
cd ../..
rm -r -f -v build
exit 0
