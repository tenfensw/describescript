class NilClass
  def clone
    return nil
  end
end

class Fixnum
  def clone
    return self
  end
end
