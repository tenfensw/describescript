class Array
	def include_any?(el)
		self.each do |element|
			if element == el then
				return true
			end
		end
		return false
	end

	def join_ml(sep)
		out = ''
		self.each do |item|
			item_s = item.to_s
			if item_s.include? ' ' then
				item_s = '"' + item_s.gsub('"', '\"') + '"'
			end
			if out == '' then
				out = item_s
			else
				out += sep + item_s
			end
		end
		return out
	end

	def include_key?(key, val)
		self.each do |hash|
			if Hash === hash && hash[key].to_s == val.to_s then
				return true
			end
		end
		return false
	end

	def shift_l(num=1)
		self.shift(num)
		return self
	end

	def before(item)
		out = []
		self.each do |ritem|
			if ritem == item then
				return out
			else
				out.push(ritem)
			end
		end
		return out
	end

	def find_with_key(key, value)
		self.each do |hash|
			if Hash === hash && hash[key.to_s].to_s == value.to_s then
				return hash
			end
		end
		return []
	end
	
	def gsub_everywhere(listing)
		out = []
		self.each do |item|
			if String === item then
				out_item = item.clone
				listing.each do |v|
					out_item = out_item.gsub(v[0], v[1]).clone
				end
				out.push(out_item)
			end
		end
		return out
	end

	def after(item)
		out = []
		can_add = false
		self.each do |ritem|
			if ritem == item then
				can_add = true
			end
			if can_add then
				out.push(ritem)
			end
		end
		out.shift
		return out
	end
end

class String
	def is_maths?
		allowed_chars = [ '+', '-', '/', '*', '(', ')' ]
		self.split('').each do |charact|
			if not (charact.to_i.to_s == charact || allowed_chars.include?(charact)) then
				return false
			end
		end
		return true
	end

	def get_all_occurences(txtop, txtcl)
		index = -1
		out = []
		self.length.times do
			index += 1
			if self[index..self.length - 1].downcase.start_with? txtop.downcase then
				out.push({ 'start' => index.clone, 'end' => -1 })
			elsif self[index..self.length - 1].downcase.start_with? txtcl.downcase then
				if out[-1] == nil then
					return []
				end
				out[-1]['end'] = index.clone
				out.unshift(out.pop)
			end
		end
		return out.reverse
	end

	def get_all_commands
		cmds = []
		self.get_all_occurences('%{', '}').each do |cmd|
			in_hash = { 'start' => cmd['start'], 'end' => cmd['end'], 'cmd' => self[cmd['start'] + 2..cmd['end'] - 1].clone }
			cmds.push(in_hash)
		end
		return cmds
	end

	def replace_indices(startv, endv, ctnt)
		start_str = self[0..startv - 1].to_s
		end_str = self[endv + 1..self.length - 1].to_s
		return start_str + ctnt.clone.to_s + end_str
	end

	def split_preserve_quotes(sep)
		out = []
		word = ''
		inside_quotes = false
		quotes_type = '"'
		index = -1
		self.split('').each do |charact|
			index += 1
			if charact == sep && inside_quotes == false then
				out.push(word.clone)
				word = ''
			elsif charact == '"' || charact == '\'' then
				inside_quotes = (not inside_quotes)
			else
				word += charact
			end
		end
		if word != '' then
			out.push(word)
		end
		return out
	end
end
