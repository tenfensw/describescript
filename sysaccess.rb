def ds_fetchos
	if ENV['OS'] == 'Windows_NT' then
		return 'win32'
	elsif `uname`.include? 'Linux' then
		return 'linux'
	elsif `uname`.include? 'Darwin' then
		return 'macos'
	else
		return 'posix'
	end
end
